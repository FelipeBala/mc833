#include "headers.h"




Interface::Interface(){
	this->reset();

}


void Interface::reset(){
	int i;
	for(i=0;i<256;i++){
		this->letra[i] = true;
	}
	this->continuarNoJogo = true;
}



int Interface::menuPrincipal(){
        char tecla, s[10];
        while(true){
                printf("\n\nBem vindo ao jogo da forca!\n"
                "-----\n\n"
                "1) Iniciar partida simples\n"
                "2) Ser carrasco ao iniciar partida\n"
                "3) jogar no modo multiplayer\n\n"
                "Escolha>");

                scanf("%s", s);
                tecla = s[0];
                switch(tecla){
                        case '1': this->tipoDeJogo = Simples; return 1;
                        case '2': this->tipoDeJogo = Carrasco; return 2;
                        case '3': this->tipoDeJogo = Multiplayer; return 3;
                }
        }

    perror("Erro Interface::menuPrincipal");
    exit(1);

}


void Interface::menuIPServidor(char* ip){
        do{
                printf("Entre com o IP do Servidor: ");
                scanf("%s", ip);
                if(strlen(ip)>16){
                        printf("IP inv�lido. Tente novamente. \n\n");
                }
        }while(strlen(ip)>16);
        return;
}

void Interface::statusServidor(bool servidorUP){
        if(servidorUP){
                printf("Servidor Online.\n");
        }else{
                printf("Servidor Offline.\n");
        }
}

bool Interface::falhaServidorTentarNovamente(){
        char resposta[100];
        while(true){
                printf("Tentar conectar novamente ? [S]im ou [N]ao\n");
                printf("Escolha>>");
                scanf("%s", resposta);
                if(resposta[0]=='s' || resposta[0]=='S'){return true;}
                if(resposta[0]=='n' || resposta[0]=='N'){return false;}
        }
        return false;

}


void Interface::digiteNome(char *nome){
	char temp[500];
    while(true){
    	printf("Digite o seu nome: ");
    	scanf("%s", temp);
    	if(strlen(temp) > 48){
    		printf("Nome muito longo.");
    		continue;
    	}else{
    		strcpy(nome, temp);
    		return;
    	}
    }

}


void Interface::atualizaJogo(){
	unsigned int i,j;
	std::system("clear");

	printf("Palavra:   ");
	j = strlen(this->atualizaoDoJogo.palavra);
	for(i=0;i<j;i++){
		printf("%c ",this->atualizaoDoJogo.palavra[i]);
	}
	printf("\n\n");

	j = (unsigned int) this->atualizaoDoJogo.letra;
	this->letra[j] = false;

	printf("----------------------------------------------\n");
	for(i=65; i<91; i++){
		if(this->letra[i]){
			printf("| %c ",i);
		}else{
			printf("|   ");
		}
		if((i-64)%10 == 0){
			printf("|\n");
		}
	}
	printf("|\n");
	printf("----------------------------------------------\n");
	if(this->tipoDeJogo != Simples){
		printf("Esta na vez do jogador: %s\n", this->atualizaoDoJogo.proximaPessoa);

	}
	printf("Vidas:     %d\n",this->atualizaoDoJogo.vidas);
	
	printf("Servidor: %s\n",  this->atualizaoDoJogo.msgServidor);

	printf(" ");
	
	if(this->atualizaoDoJogo.vidas <= 0){
		continuarNoJogo = false;
		this->perdeuJogo();
	}

	if(this->atualizaoDoJogo.tamanhoPalavra <= 0){
		continuarNoJogo = false;
		this->ganhouJogo();
	}

}

void Interface::perdeuJogo(){
	std::system("clear");
	printf(	"\n\nA Voce perdeu !!! \n\n"
			"-----\n"
			"A palavra era: %s\n"
			"-----\n\n"
			"[Tecle] para voltar ao menu\n", this->atualizaoDoJogo.palavra);
	getchar();
	//getchar();
	std::system("clear");

}

void Interface::ganhouJogo(){
	std::system("clear");
	printf(	"\n\nA Parabens !!! Voce ganhou !!! \n\n"
			"-----\n"
			"A palavra era: %s\n"
			"-----\n\n"
			"[Tecle] para voltar ao menu\n", this->atualizaoDoJogo.palavra);
	getchar();
	getchar();
	std::system("clear");

}


void Interface::MenssagemInicial(){
	std::system("clear");
	printf(	"\n\nA partida do jogo da forca comecou!\n"
			"-----\n"
			"Voce possui %d vidas.\n"
			"A palavra possui %d caracteres\n\n"
			"[Tecle] para iniciar\n", this->atualizaoDoJogo.vidas, this->atualizaoDoJogo.tamanhoPalavra);
	getchar();
	getchar();
}


void Interface::mensagemSemCarrasco(){
	std::system("clear");
	printf(	"\n\nJogo Encerrado"
			"Ninguem quis ser o carrasco !\n"
			"-----\n"
			"[Tecle] para voltar ao menu\n");
	getchar();
	getchar();
}






void Interface::help(){
	std::system("clear");
	printf("HELP - JOGO DA VELHA\n");
	printf("Para iniciar o jogo digite no terminal ./cliente e insira o IP do servidor\n");
	printf("Ou digite no terminal ./cliente IP_Servidor\n\n");
	printf("O jogo da forca consiste em tentar adivinhar a palavra oculta \n");
	printf("Voc� pode jogar no modo singleplayer onde voce jogara contra o servidor\n");
	printf("Ou voce pode jogar no modo multiplayer contra outros jogadores\n\n");
	printf("Regras:\n");
	printf("Nao sao permitidos caracteres especiais nas palavras.\n");
	printf("Voce possui 3 vidas no modo singleplayer\n");
	printf("Voce possui 6 vidas no modo multiplayer\n");

	printf("\n\nCRIADO POR: FELIPE BALABANIAN - 140603\n");




}



Interface::~Interface(){
}
