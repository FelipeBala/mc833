
#include "headers.h"


RedeServidor::RedeServidor(){}

/////////////////  UNIX SOCKET  (servidor) ////////////////

void RedeServidor::levantaControlador(){
	pid_t pid;
	if((pid = fork()) == 0) {
		this->controlador();
		exit(-1);
	}

	if((pid = fork()) == 0) {
		this->dicionario();
		exit(-1);
	}
	return;
}

void RedeServidor::controlador(){
	#define MaxSessions 100
	struct pollfd ufds[MaxSessions];
	char msg[maxPacketLen];
	int i,n,rv;
	int unixClient;
	int unixServidor = criarServidorUnix("./Controlador");

	printf("Servidor Controlador Montado.\n");

	this->zera(ufds);

	ufds[0].fd = unixServidor;
	ufds[0].events = POLLRDNORM;

	this->indexSessao=1;
	this->existeCarrasco = false;
	this->clientesNaSessao = 0;

	while(true){
		// Espera por um ou mais evento dos descritores selecionados. Se a espera alcancar 1 segundo
		// indica um timeout retornando 0 para a variavel rv.
		rv = poll(ufds, MaxSessions, 1000);
		if (rv == -1) {
			// Ocorreu um erro no poll
			printf("Erro no poll\n");}
		else if (rv == 0) {
			//Time-Out
			//printf("Timeout !  Sem dados apos espera de %d segundos.\n",TempoEspera);'
		  }
		else {
			// Verifica os eventos

			//Verifica o servidor para novas conexoes.
			if (ufds[0].revents & POLLRDNORM) {
				if ( (unixClient = accept(unixServidor, NULL, NULL)) == -1) {
				  printf("Controlador - accept error\n");
				  continue;
				}
				this->coloqueNoProximoVago(ufds, unixClient, MaxSessions);
			}

			//Verifica eventos dos clientes.
			for(i=1;i<MaxSessions;i++){
				if(ufds[i].fd < 0){continue;}
				if (ufds[i].revents & POLLRDNORM){
					if( (n=read(ufds[i].fd, msg, maxPacketLen-1)) <= 0){
						Close(ufds[i].fd);
						ufds[i].fd = -1;
					}else{
						msg[n]=0;
						this->controleDeSessao(ufds[i].fd, msg);
					}

				}

			}

		}

	}


}

void RedeServidor::zera(struct pollfd *ufds){
	int i;
	for(i=0;i<MaxSessions;i++){
		ufds[i].fd=-1;
	}
}

bool RedeServidor::coloqueNoProximoVago(struct pollfd *ufds, int fd, int max){
	int i;
	for(i=1;i<max;i++){
		if(ufds[i].fd < 0){
			ufds[i].fd = fd;
			ufds[i].events = POLLRDNORM;
			return true;
		}
	}
	return false;
}


void RedeServidor::controleDeSessao(int fd, char*msg){
	pid_t pid;
	bool iniciarNovaSessao=false;
	char sessao[maxPacketLen];
	#define TempoDeEsperaParaIniciarJogo 40



	sprintf(sessao,"Sessao_%d:",this->indexSessao);

	printf("controleDeSessao, recebe msg: %s\n", msg);

	if(!strcmp(msg,"QueroSerCarrasco")){
		if(!this->existeCarrasco){
			strcat(sessao,"VoceCarrasco#");
			this->existeCarrasco = true;
			this->clientesNaSessao++;
			iniciarNovaSessao=true;
		}else{
			strcat(sessao,"CarrascoJaEscolido#");
		}
	}

	if(!strcmp(msg,"QueroJogarMultiplayer")){
		strcat(sessao,"VoceMultiplayer#");
		this->clientesNaSessao++;
		iniciarNovaSessao=true;
	}

	if(!strcmp(msg,"Tempo")){
		sprintf(sessao, "Sessao_%d:Tempo#Tempo:%ld#Jogadores:%d#", this->indexSessao, this->tempoIniciarPartida-time(NULL), this->clientesNaSessao);
	}



	if(iniciarNovaSessao && this->clientesNaSessao==1){
		this->tempoIniciarPartida = time(NULL)+TempoDeEsperaParaIniciarJogo;
		if((pid = fork()) == 0) {
			this->controladorDeUmaSessao();
			//exit(-1);
			exit(0);
		}
	}

	printf("controleDeSessao, envia msg: %s\n", sessao);
	
	this->responderUnix(fd, sessao);


	if(this->tempoIniciarPartida - time(NULL) < 0 || this->clientesNaSessao > 6){
		this->indexSessao++;
		printf("Iniciando nova sessao %d", this->indexSessao);		
		this->existeCarrasco = false;
		this->clientesNaSessao = 0;
	}


}



void  RedeServidor::controladorDeUmaSessao(){
	# define maxClientes 10
	struct pollfd ufds[maxClientes];
	char msg[maxPacketLen];
	int i,j,n,rv;
	int unixClient;
	char sessao[maxPacketLen];



	sprintf(sessao,"./Sessao_%d",this->indexSessao);
	int unixServidor = criarServidorUnix(sessao);

	printf("Servidor %s Montado.\n", sessao);

	this->zera(ufds);

	ufds[0].fd = unixServidor;
	ufds[0].events = POLLRDNORM;


	while(true){
		// Espera por um ou mais evento dos descritores selecionados. Se a espera alcancar 1 segundo
		// indica um timeout retornando 0 para a variavel rv.
		rv = poll(ufds, 100, 1000);
		if (rv == -1) {
			// Ocorreu um erro no poll
			printf("Erro no poll - %s\n", sessao);}
		else if (rv == 0) {
			//Time-Out
			//printf("Timeout !  Sem dados apos espera de %d segundos.\n",TempoEspera);'
		  }
		else {
			// Verifica os eventos

			//Verifica o servidor para novas conexoes.
			if (ufds[0].revents & POLLRDNORM) {
				if ( (unixClient = accept(unixServidor, NULL, NULL)) == -1) {
				  printf("%s - accept error\n", sessao);
				  continue;
				}
				this->coloqueNoProximoVago(ufds, unixClient, maxClientes);
			}

			for(i=1;i<maxClientes;i++){
				if(ufds[i].fd < 0){continue;}
				if (ufds[i].revents & POLLRDNORM){
					if( (n=read(ufds[i].fd, msg, maxPacketLen-1)) <= 0){
						Close(ufds[i].fd);
						ufds[i].fd = -1;
					}else{
						msg[n]=0;

						for(j=1;j<maxClientes;j++){
							if(ufds[i].fd < 0 || ufds[i].fd==ufds[j].fd){continue;}
							RedeServidor::responderUnix(ufds[j].fd, msg);
						}

					}

				}

			}


		}

	}



}



////////// Gerenciador de sessao (Cliente) //////////////////

bool RedeServidor::perguntaCarrasco(){
	int n=-1;
	int clienteUnix = criarClienteUnix("./Controlador");
	const char *m ="QueroSerCarrasco";
	char msg[maxPacketLen],res[maxPacketLen],val[maxPacketLen];

	while(n<1){
		if ( write(clienteUnix, m, strlen(m)) > 0 ){
			printf("perguntaCarrasco - Palavra enviada: %s\n",m);
		}else{
			printf("perguntaCarrasco - Erro de envio\n");
		}

		if ( (n = read(clienteUnix, msg, maxPacketLen-1) ) > 0 ){
			printf("perguntaCarrasco - Palavra recebido: %s\n",msg);
			msg[n]=0;
		}else{
			printf("perguntaCarrasco - Erro de recebimento\n");
		}
	}

	if( parseRede(msg,res,val) == 0){return false;}

	this->indexSessao = atoi(res+7);

	if(!strcmp(val,"VoceCarrasco")){return true;}
	if(!strcmp(val,"CarrascoJaEscolido")){return false;}

	printf("Erro em perguntaCarrasco - %s",msg);
	return false;

}

bool RedeServidor::perguntaMultiplayer(){
	int n=-1;
	int clienteUnix = criarClienteUnix("./Controlador");
	const char *m ="QueroJogarMultiplayer";
	char msg[maxPacketLen],res[maxPacketLen],val[maxPacketLen];

	while(n<1){
		if ( write(clienteUnix, m, strlen(m)) > 0 ){
			printf("perguntaMultiplayer - Palavra enviada: %s\n",m);
		}else{
			printf("perguntaMultiplayer - Erro de envio\n");
		}

		if ( (n = read(clienteUnix, msg, maxPacketLen) ) > 0 ){
			printf("perguntaMultiplayer - Palavra recebido: %s\n",msg);
			msg[n]=0;
		}else{
			printf("perguntaMultiplayer - Erro de recebimento\n");
		}
	}

	if( parseRede(msg,res,val) == 0){return false;}

	//strcpy(sessaoAtual, res);
	this->indexSessao = atoi(res+7);

	if(!strcmp(val,"VoceMultiplayer")){return true;}

	printf("Erro em perguntaCarrasco - %s",msg);
	return false;

}



////////// Server Dicionario //////////////////////////////


void RedeServidor::dicionario(){
	std::string line, palavra;
	char resposta[maxPacketLen];
	pid_t pid;
	std::ifstream myfile ("dicionario.txt");
	int n;

	if (myfile.is_open()){
		while ( getline (myfile,line) )
	    {
	      this->dicionarioDePalavras.push_back(line);
	    }
	    myfile.close();
	}else{
		  printf("Nao foi possivel abrir o arquivo de dicionario.\n");
	}

	int unixServidor = criarServidorUnix("./Dicionario");
	int unixClient;
	
	printf("Servidor Dicionario Montado.\n");

	while(true){
		if ( (unixClient = accept(unixServidor, NULL, NULL)) == -1) {
			printf("Dicionario - accept error\n");
			continue;
		}

		//Trocar por thread
		if((pid = fork()) == 0) {

			Close(unixServidor);
			//Independentemente da requisacao, envia uma palavra;
			read(unixClient, resposta, maxPacketLen-1);

			this->sorteiaPalavra(palavra);

			if ( (n = write(unixClient, palavra.c_str(), palavra.length()) ) > 0 ){
				printf("Dicionario - Palavra enviada: %s\n",palavra.c_str());
			}else{
				printf("Erro de envio em enviarMsg\n");
			}
			Close(unixClient);
			exit(0);

		}
		Close(unixClient);
	}


}

void RedeServidor::sorteiaPalavra(std::string &palavra){
	srand (time(NULL));
	palavra = this->dicionarioDePalavras[rand() % this->dicionarioDePalavras.size()];
}


void RedeServidor::buscaPalavra(char *palavra){
	int n=-1;
	int clienteUnix = criarClienteUnix("./Dicionario");
	const char *m ="BuscaPalavra";

	while(n<1){
		if ( write(clienteUnix, m, strlen(m)) > 0 ){
			printf("buscaPalavra - Palavra enviada: %s\n",m);
		}else{
			printf("buscaPalavra - Erro de envio\n");
		}

		if ( (n = read(clienteUnix, palavra, 50) ) > 0 ){
			printf("buscaPalavra - Palavra recebido: %s\n",m);
		}else{
			printf("buscaPalavra - Erro de recebimento\n");
		}
	}

}


/////////////////  TCP SOCKET  ////////////////


int RedeServidor::levantaServidor(int porta){

	int listenfd,connfd;
	pid_t pid;

	listenfd = Socket(AF_INET, SOCK_STREAM, 0);
	this->servidorAddr = ServerSockaddrIn(AF_INET, INADDR_ANY, porta);
	
	if( Bind(	listenfd, 
				(struct sockaddr *) &(this->servidorAddr), 
				sizeof(this->servidorAddr)) == -1){return -1;}
				
	Listen(listenfd, 20);


	while(true) {

	  struct sockaddr_in clientaddr;
	  socklen_t clientaddr_len = sizeof(clientaddr);

	  connfd = Accept(listenfd, (struct sockaddr *) &clientaddr, &clientaddr_len);

	  if((pid = fork()) == 0) {
		 Close(listenfd);
		 this->clienteaddr = clientaddr;
		 this->connfd = connfd;
		 return connfd;
		 //Close(connfd);
	  }
	  waitpid(-1, NULL, WNOHANG);
	  Close(connfd);
	}

	return(0);
}

bool RedeServidor::esperaMensagem(char* menssagem){
	int n;
	if((n = read(this->connfd, menssagem, maxPacketLen-1)) > 0) {
	  menssagem[n] = 0;
	  printf("Mensagem do cliente: %s\n", menssagem);
	  return true;
	}
	return false;
}


Acao RedeServidor::identificaTipoJogo(char *menssagem){
	char res[maxPacketLen],val[maxPacketLen];
	unsigned int lentxt = strlen(menssagem);

	while(lentxt > 0){
		lentxt -= parseRede(menssagem,res,val);
		printf("parseado:_%s_%s_\n",res,val);

		if(!strcmp(res,"EscolhaDoJogo")){
			if(!strcmp(val,"Simples")){return Acao::IniciarJogoSimples;}
			if(!strcmp(val,"Carrasco")){return Acao::IniciarJogoCarrasco;}
			if(!strcmp(val,"Multiplayer")){return Acao::IniciarJogoMultiplayer;}
			printf("identificaTipoJogo - erro 1\n");
			return Acao::ErroServidor;
		}
		printf("identificaTipoJogo - erro 2\n");
		return Acao::ErroServidor;
	}
printf("identificaTipoJogo - erro 3\n");
return Acao::ErroServidor;

}


bool RedeServidor::responder(const char *resposta){
	if(write(this->connfd, resposta, strlen(resposta)) > 0){
		printf("Resposta do servidor: %s\n", resposta);
	}else{
		printf("Erro em RedeServidor::responder");
	}
return true;
}

bool RedeServidor::responderUnix(int fd, const char *resposta){
	if(write(fd, resposta, strlen(resposta)) > 0){
		printf("responderUnix: %s\n", resposta);
	}else{
		printf("Erro em RedeServidor::responderUnix");
	}
return true;
}

//////////////////////////////////// Tratamento das Msg do Cliente //////////////////////////////////////////////////

void RedeServidor::inicializaEstadoJogo(DadosJogo &estadoJogo, char *palavra, ModalidadeJogo jogo){
	unsigned int l = strlen(palavra);
	//printf("Palavra de tamanho %d letras",l);
	estadoJogo.correta = false;
	estadoJogo.letra = '_';
	estadoJogo.tamanhoPalavra = l;
	strcpy(estadoJogo.pessoaQueEscolheu,"_");
	strcpy(estadoJogo.proximaPessoa,"_");
	if(jogo == Simples){
		estadoJogo.vidas =3; }
	else{
		estadoJogo.vidas =6;
		}
	unsigned int i;
	for(i=0; i<l ;i++){
		estadoJogo.palavra[i]='_';
	}
	estadoJogo.palavra[i]=0;
	strcpy(estadoJogo.msgServidor, "boa partida.");

	for(i=0;i<256;i++){
		this->letra[i] = true;
	}
}


void RedeServidor::verificaSePalavraPossuiLetra(DadosJogo &estadoJogo, char *palavra, char val){
	unsigned int l = strlen(palavra);
	unsigned int i,j;
	bool perdeVida=true;

	estadoJogo.letra = val;

	val = toupper(val);
	estadoJogo.letra = val;

	j = (unsigned int) val;

	if(letra[j]){
		for(i=0;i<l;i++){
			if(palavra[i]==val){
				perdeVida=false;
				letra[j] = false;
				estadoJogo.correta = true;
				estadoJogo.palavra[i] = val;
				estadoJogo.tamanhoPalavra--;
				strcpy(estadoJogo.msgServidor,"Letra certa");
			}
		}
	}else{
		perdeVida=false;
		estadoJogo.correta = false;
		sprintf(estadoJogo.msgServidor, "A letra '%c' ja foi utilizada", val);
	}

	if(perdeVida){
		estadoJogo.vidas--;
		estadoJogo.correta = false;
		sprintf(estadoJogo.msgServidor, "A palavra nao tem nenhuma letra '%c'.", val);
	}


}

bool RedeServidor::validaLetra(DadosJogo &estadoJogo, char val){
	unsigned int j;
	j = (unsigned int) val;
	if(j<65 || j >90){
		estadoJogo.correta = false;
		estadoJogo.vidas--;
		strcpy(estadoJogo.msgServidor,"Letra invalida");
		return false;
	}
	return true;

}

bool RedeServidor::parseMsgDoCliente(DadosJogo &estadoJogo, char *palavra, char *resposta){
	char res[maxPacketLen],val[maxPacketLen];
	unsigned int lentxt = strlen(resposta);
	unsigned int i;
	while(lentxt > 0){
		if( (i = parseRede(resposta,res,val)) == 0){return false;}
		lentxt -= i;
		resposta = resposta+i;
		printf("parseMsgDoCliente %d %s %s\n",i, res,val);

		if(!strcmp(res,"Letra")){
			if(!strcmp(val,"$")){
				estadoJogo.letra = '$';
				estadoJogo.correta = false;}
			else{
				if(validaLetra(estadoJogo, val[0])){
					verificaSePalavraPossuiLetra(estadoJogo,palavra, val[0]);
				}
			}
			continue;
		}

		/*
		if(!strcmp(res,"palavra")){
			strcpy(atualiza.palavra,val);
			continue;
		}

*/



		return false;
	}

return true;






}

void RedeServidor::parseEstadoJogo(DadosJogo &estadoJogo, char *message){
	char t[10];
	strcpy(message, "correta:");
	if(estadoJogo.correta){strcat(message, "Y#");}else{strcat(message, "N#");}
	strcat(message, "letra:");
	t[0]=estadoJogo.letra;
	t[1]='#';
	t[2]=0;
	strcat(message, t);
	strcat(message, "palavra:");
	strcat(message, estadoJogo.palavra);
	strcat(message, "#");
	strcat(message, "tamanhoPalavra:");
	sprintf(t,"%d",estadoJogo.tamanhoPalavra);
	strcat(message, t);
	strcat(message, "#");
	strcat(message, "pessoaQueEscolheu:");
	strcat(message, estadoJogo.pessoaQueEscolheu);
	strcat(message, "#");
	strcat(message, "proximaPessoa:");
	strcat(message, estadoJogo.proximaPessoa);
	strcat(message, "#");
	strcat(message, "vidas:");
	sprintf(t,"%d",estadoJogo.vidas);
	strcat(message, t);
	strcat(message, "#");
	strcat(message, "msgServidor:");
	strcat(message, estadoJogo.msgServidor);
	strcat(message, "#");
}



void RedeServidor::tratamento(DadosJogo &estadoJogo, char* palavra, ModalidadeJogo jogo){


	char   message[maxPacketLen];
	char   response[maxPacketLen];
	int    n, rv;
	bool sair=false;
	int p;

	// Estrutura de dados para definir quais decritores o poll ira tratar e para quais
	// Eventos ele ficara atento.
	struct pollfd ufds[2];
	ufds[0].fd = this->connfd;
	ufds[0].events = POLLIN | POLLPRI;

	if(jogo == Simples){
		 p=1;
	}else{
		p=2;
		sprintf(sessao,"Sessao_%d",this->indexSessao);
		printf("tratamentoPreJogo - %s\n",sessao);
		int connUnix = criarClienteUnix(sessao);
		ufds[1].fd = connUnix;
		ufds[1].events = POLLIN ;

	}


	while(true){
	// Espera por um ou mais evento dos descritores selecionados. Se a espera alcancar 1 segundo
	// indica um timeout retornando 0 para a variavel rv. Em caso de erro retorna 0.
		rv = poll(ufds, p, 100);
		if (rv == -1) {
			// Ocorreu um erro no poll
			printf("Erro no poll\n");}
		else if (rv == 0) {
			//Time-Out
			//printf("Timeout !  Sem dados apos espera de %d segundos.\n",TempoEspera);'
		  }
		else {
			// Verifica os eventos
			// Verifica se ha pacotes para ler no socket.
			if (ufds[0].revents & POLLIN) {
				if( (n = read(this->connfd, response, maxPacketLen-1)) > 0){
					response[n]=0;
					printf("Tratamento:  %s\n", response);
					parseMsgDoCliente(estadoJogo, palavra, response);
					sair = perdeuJogo(estadoJogo, palavra);
					sair = sair | ganhouJogo(estadoJogo);
					parseEstadoJogo(estadoJogo, message);
					printf("Tratamento - enviando para o cliente:  %s\n", message);
					responder(message);
					if(sair){return;}
				}else{
					return;
				}
			}

			if (jogo != Simples && ufds[1].revents & POLLIN) {
				if( (n = read(this->connfd, response, maxPacketLen-1)) > 0){
					responder(response);
				}

			}

		}

	}

}

bool RedeServidor::perdeuJogo(DadosJogo &estadoJogo, char* palavra){

	if(estadoJogo.vidas <= 0){
		printf("Vidas do Jogador zeradas\n");
		strcpy(estadoJogo.palavra,palavra);
		return true;
	}
	return false;
}


bool RedeServidor::ganhouJogo(DadosJogo &estadoJogo){

	if(estadoJogo.tamanhoPalavra <= 0){
		printf("Jogador ganhou o jogo\n");
		return true;
	}
	return false;
}


bool RedeServidor::tratamentoPreJogo(DadosJogo &estadoJogo, char* palavra){

	char res[maxPacketLen],val[maxPacketLen];
	unsigned int lentxt;
	unsigned int i;
	char   response[maxPacketLen];
	char *resposta;
	int    n, rv;
	int jogadores,tempo;



	char sessao[maxPacketLen];
	sprintf(sessao,"Sessao_%d",this->indexSessao);
	printf("tratamentoPreJogo - %s\n",sessao);
	int connUnix = criarClienteUnix(sessao);
	int connUnixContr = criarClienteUnix("./Controlador");


	// Estrutura de dados para definir quais decritores o poll ira tratar e para quais
	// Eventos ele ficara atento.
	struct pollfd ufds[3];
	ufds[0].fd = this->connfd;
	ufds[0].events =  POLLIN;
	ufds[1].fd = connUnix;
	ufds[1].events = POLLIN;
	ufds[2].fd = connUnixContr;
	ufds[2].events = POLLIN;


	strcpy(palavra, "TESTE");

	while(true){
	// Espera por um ou mais evento dos descritores selecionados. Se a espera alcancar 1 segundo
	// indica um timeout retornando 0 para a variavel rv. Em caso de erro retorna 0.
		rv = poll(ufds, 3, 1000);
		if (rv == -1) {
			// Ocorreu um erro no poll
			printf("Erro no poll\n");}
		else if (rv == 0) {
			//Time-Out
			//printf("Timeout !  Sem dados apos espera de %d segundos.\n",TempoEspera);'
		  }
		else {
			
			//printf("CASA0\n");
			// Verifica os eventos
			// Verifica se ha pacotes para ler no socket.
			if (ufds[0].revents & POLLIN) {
				if( (n = read(ufds[0].fd, response, maxPacketLen-1)) > 0){
					response[n]=0;
					
					printf("CASA1\n");
					if(!strcmp(response,"Tempo")){
						printf("CASA2\n");
						responderUnix(connUnixContr, "Tempo");
					}

				}

			}


			if (ufds[1].revents & POLLIN) {
				printf("CASA3\n");				
				if( (n = read(ufds[1].fd, response, maxPacketLen-1)) > 0){
					response[n]=0;
					strcpy(palavra, response);

				}

			}

			if (ufds[2].revents & POLLIN) {
				if( (n = read(ufds[2].fd, response, maxPacketLen-1)) > 0){
					response[n]=0;
					resposta = response;
					lentxt = strlen(response);
					printf("CASA4\n");
					
					if( (i = parseRede(resposta,res,val)) == 0){continue;}
					if(strcmp(res,sessao)){
							if(palavra[0] != 0){
								responder("IniciarPartidaMultiplayer");
								Close(connUnix);
								Close(connUnixContr);
								return true;
							}else{
								responder("SemCarrasco");
								Close(connUnix);
								Close(connUnixContr);
								return false;
							}
					}
					while(lentxt > 0){
						if( (i = parseRede(resposta,res,val)) == 0){continue;}
						lentxt -= i;
						resposta = resposta+i;
						//printf("tratamentoPreJogo %d %s %s\n",i, res,val);
						
						if(!strcmp(res,"Tempo")){
							tempo = atoi(val);
						}
						if(!strcmp(res,"Jogadores")){
							jogadores = atoi(val);
						}
					}
				}
				printf("CASA4\n");
				sprintf(response, "Faltam %d para a partida iniciar.\n Ha %d jogadores.", tempo,jogadores);
				responder(response);

			}

		}

	}




}







