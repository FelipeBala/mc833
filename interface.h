#ifndef mc833_interface
#define mc833_interface

#include "redeServidor.h"




class Interface {
	
	public:
	

	ModalidadeJogo tipoDeJogo;
	DadosJogo atualizaoDoJogo;
	bool letra[256];
	bool continuarNoJogo;


	Interface();
	void reset();
	int menuPrincipal();	
	void menuIPServidor(char*);
	void statusServidor(bool);
	bool falhaServidorTentarNovamente();
	void atualizaJogo();
	void perdeuJogo();
	void ganhouJogo();
	void help();
	void MenssagemInicial();
	void digiteNome(char *nome);
	void mensagemSemCarrasco();



	
	~Interface();
	
	private:
	std::vector<char*> lixo;
	
};

#endif
