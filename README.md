# MC833 - Projeto Final

O projeto final da mat�ria MC833 consistiu em montar um jogo da velha com dois modos de jogo: um singleplayer onde o cliente joga contra o servidor e outro multiplayer onde um cliente denominado carrasco escolhe a palavra e os outros tentam adivinhar a palavra antes de perderem todas as suas vidas. 

## Come�ando 

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Pr�-Requisitos

� necess�rio ter G++ que suporte std=c++11 ou maior.

### Instala��o

Digite no terminal make all

```
make all
```

## Jogar

Para jogar � preciso ter ou conhecer um servidor do jogo.

Se voc� conhece o endere�o ip de algum servidor do jogo, basta iniciar o jogo com este endere�o IP.  

```
./cliente 127.0.0.1
```
Ou 

```
./cliente
Digite o endere�o IP do servidor:
127.0.0.1
```

Caso voc� n�o conhe�a um endere�o IP, ent�o ser� necess�rio criar o seu pr�prio servidor.

```
./servidor
```

e depois se conectar a ele

```
./cliente 127.0.0.1
```

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versionamento

Todo o c�digo � versionado no Bitbucket.
https://bitbucket.org/FelipeBala/mc833.git

## Authors

* **Felipe Balabanian** - *140603*

## Licen�a

Free, all free and public.



