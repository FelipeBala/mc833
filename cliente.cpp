#include "headers.h"

int main(int argc, char** argv){
	char ip[17], nome[50];
	//char senha[30],teclado[50];
	bool servidorUP;
	

	RedeCliente rede = RedeCliente();
	Interface intf = Interface();

	if (argc == 1) {
		//Usuario entra com o IP do servidor
		intf.menuIPServidor(ip);
	}else{
		if(!strcmp(argv[1],"-h")){
			//Verifica se argumento eh um pedido de ajuda.
			intf.help();
		}else{
			//IP foi passado pelo argumento.
			strcpy(ip,argv[1]);
		}
	}

	//Tenta se conectar ao servidor, (max de 3 vezes com 1 segundo de intervalo)
	servidorUP = rede.conectarServidor(ip, 3001);
	//Informa status do Servidor
	intf.statusServidor(servidorUP);

	//Se o cliente nao se conectou ao cliente, pergunta se o usuario quer tentar denovo
	bool T = false;
	while(!servidorUP && T){
		T = intf.falhaServidorTentarNovamente();
		//Tenta se conectar ao servidor
		servidorUP = rede.conectarServidor(ip, 3001);
		//Informa status do Servidor
		intf.statusServidor(servidorUP);
		
	}
	//Cliente conectado com o servidor


	//Acessa as credenciais

	strcpy(nome, "Felipe");
	//intf.digiteNome(nome);


	while(true){
		//Mostra o menu principal para o usuario escolher o tipo de jogo
		intf.menuPrincipal();

		//Envia escolha do jogo para o servidor. Se proposta falhar volta para o menu principal.
		if( !rede.enviarEscolhaJogo(intf.tipoDeJogo) ){continue;}
		if(intf.tipoDeJogo == Simples){
			//Pede pro servidor os dados do jogo.
			rede.requerirAtualizacao(intf.atualizaoDoJogo);
			//Mostra a Tela Inicia do jogo.
			intf.MenssagemInicial();
			//Renderiza a tela do jogo com as atualizacoes
			intf.atualizaJogo();
			while(intf.continuarNoJogo){
				//Espera por eventos do usuario ou do servidor.
				//Atualizando o estdo do jogo com as resposta do servidor.
				intf.continuarNoJogo = intf.continuarNoJogo & rede.terminal(intf.atualizaoDoJogo);
				//Renderiza a tela do jogo com as atualizacoes
				intf.atualizaJogo();
				//usleep(1000*1000);
			}
		}

		/*
		if(intf.tipoDeJogo == Carrasco){
			//Pede pro servidor os dados do jogo.
			rede.requerirAtualizacao(intf.atualizaoDoJogo);
			//Mostra a Tela Inicia do jogo.
			intf.MenssagemInicial();
			//Renderiza a tela do jogo com as atualizacoes
			intf.atualizaJogo();
			while(intf.continuarNoJogo){
				//Espera por eventos do usuario ou do servidor.
				//Atualizando o estdo do jogo com as resposta do servidor.
				intf.continuarNoJogo = intf.continuarNoJogo & rede.terminal(intf.atualizaoDoJogo);
				//Renderiza a tela do jogo com as atualizacoes
				intf.atualizaJogo();
				//usleep(1000*1000);
			}
		}
		*/


		if(intf.tipoDeJogo == Multiplayer){

			if(!rede.EstadoPreJogo()){
				intf.mensagemSemCarrasco();
				continue;
			}


			//Pede pro servidor os dados do jogo.
			rede.requerirAtualizacao(intf.atualizaoDoJogo);
			//Mostra a Tela Inicia do jogo.
			intf.MenssagemInicial();
			//Renderiza a tela do jogo com as atualizacoes
			intf.atualizaJogo();
			while(intf.continuarNoJogo){
				//Espera por eventos do usuario ou do servidor.
				//Atualizando o estdo do jogo com as resposta do servidor.
				intf.continuarNoJogo = intf.continuarNoJogo & rede.terminal(intf.atualizaoDoJogo);
				//Renderiza a tela do jogo com as atualizacoes
				intf.atualizaJogo();
				//usleep(1000*1000);
			}
		}

		
		intf.reset();
	}





}


