#include "headers.h"

int main(int argc, char** argv){
	RedeServidor serv = RedeServidor();

	serv.levantaControlador();
	//Levanta o listening
	if(serv.levantaServidor(3001) < 0){exit(-1);}
	


	//A partir daqui o processo s� cuidar� de um cliente.
	char menssagem[maxPacketLen];
	//char resposta[maxPacketLen];
	DadosCliente cliente;

	Acao A;

	while(true){

		do{
			if(!serv.esperaMensagem(menssagem)){return -1;}
			A =  serv.identificaTipoJogo(menssagem);
			switch(A){
				case IniciarJogoSimples:
											serv.responder("OK");
											cliente.tipoDeJogo = Simples;
											printf("JogoSimples\n");
											break;

				case IniciarJogoCarrasco:
											if(serv.perguntaCarrasco()){
												cliente.tipoDeJogo = Carrasco;
												serv.responder("VoceCarrasco");
												printf("VoceCarrasco\n");
											}else{
												A = ErroServidor;
												serv.responder("CarrascoJaEscolido");
												printf("CarrascoJaEscolido\n");
											}
											break;

				case IniciarJogoMultiplayer:
											if(serv.perguntaMultiplayer()){
												cliente.tipoDeJogo = Multiplayer;
												serv.responder("OK");
												printf("Multiplayer\n");
											}else{
												A = ErroServidor;
												serv.responder("Erro");
											}
											break;

				case ErroServidor:
											printf("Erro ao identificar Acao\n");
											break;
			}
		}while(A==ErroServidor);


		char palavra[51];
		palavra[0]=0;
		DadosJogo estadoJogo;
		printf("Iniciando variaveis\n\n\n");


		if(cliente.tipoDeJogo == Simples){
			//strcpy(palavra, "LINDO");
			serv.buscaPalavra(palavra);
			serv.inicializaEstadoJogo(estadoJogo, palavra, cliente.tipoDeJogo);
			serv.tratamento(estadoJogo,palavra);

		}

		if(cliente.tipoDeJogo == Multiplayer){
			printf("Iniciando Multiplayer\n\n\n");
			usleep(1000*1000);
			if(!serv.tratamentoPreJogo(estadoJogo, palavra)){
				printf("Partida sem carrasco\n");
				continue;
			}
			serv.inicializaEstadoJogo(estadoJogo, palavra, cliente.tipoDeJogo);
			serv.tratamento(estadoJogo,palavra,cliente.tipoDeJogo);

		}


	}






	Close(serv.connfd);
}
