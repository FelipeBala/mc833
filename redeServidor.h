#ifndef mc833_redeServidor
#define mc833_redeServidor

#include <vector>
#include <string>

enum ModalidadeJogo { Simples, Carrasco, Multiplayer };

enum Acao { IniciarJogoSimples,
			IniciarJogoCarrasco,
			IniciarJogoMultiplayer,
			ErroServidor};

typedef struct DadosCliente{
	   int    listenfd,
	          connfd,
	          port;
	   struct sockaddr_in servaddr;
	   ModalidadeJogo tipoDeJogo;

} DadosCliente;





typedef struct DadosJogo{
	char letra;  //Letra escolhida
	bool correta; //Letra correta ou n�o
	char palavra[50]; //Palavra que o clinte v�
	unsigned int tamanhoPalavra; //Letras faltantes para o cliente
	char pessoaQueEscolheu[50]; //Pessoa que escolheu a letra.
	char proximaPessoa[50]; //N�o utilizado por enquanto.
	int vidas; //Vidas do cliente
	char msgServidor[100]; //Mensagem do servidor para o cliente.
} DadosJogo;





class RedeServidor {

	public:

	struct sockaddr_in servidorAddr; //Dados do servidor
	struct sockaddr_in clienteaddr; //Dados do cliente
	int connfd; //Conexao com o cliente

	ModalidadeJogo tipoDeJogo;
	bool letra[256];


	std::vector<std::string> dicionarioDePalavras;

	//char sessaoAtual[100];

	int indexSessao;
	int clientesNaSessao;
	bool existeCarrasco;
	int tempoIniciarPartida;



	RedeServidor();

	int levantaServidor(int porta);
	bool responder(const char *resposta);
	bool esperaMensagem(char* menssagem);
	Acao identificaTipoJogo(char *menssagem);

	void inicializaEstadoJogo(DadosJogo &estadoJogo, char *palavra, ModalidadeJogo jogo);
	void verificaSePalavraPossuiLetra(DadosJogo &estadoJogo, char *palavra, char val);
	bool parseMsgDoCliente(DadosJogo &estadoJogo, char *palavra, char *response);
	void parseEstadoJogo(DadosJogo &estadoJogo, char *message);
	void tratamento(DadosJogo &estadoJogo, char* palavra, ModalidadeJogo);
	bool perdeuJogo(DadosJogo &estadoJogo, char* palavra);
	bool ganhouJogo(DadosJogo &estadoJogo);
	bool validaLetra(DadosJogo &estadoJogo, char val);




	void controlador();
	void levantaControlador();
	void zera(struct pollfd *ufds);
	bool coloqueNoProximoVago(struct pollfd *ufds, int fd, int max);
	void controleDeSessao(int fd, char*msg);
	bool perguntaCarrasco();
	bool perguntaMultiplayer();
	void controladorDeUmaSessao();
	bool responderUnix(int fd, const char *resposta);
	bool tratamentoPreJogo(DadosJogo &estadoJogo, char* palavra, ModalidadeJogo);


	void dicionario();
	void sorteiaPalavra(std::string &palavra);
	void buscaPalavra(char *palavra);


};




#endif
