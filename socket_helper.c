#include "socket_helper.h"

#include <sys/un.h>

int Socket(int domain, int type, int protocol) {
   int sockfd;

   if ((sockfd = socket(domain, type, protocol)) < 0) {
      perror("socket error");
      exit(1);
   }

   return sockfd;
}

int Bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen) {
   int result;
   
   if ((result = bind(sockfd, addr, addrlen)) == -1) {
      perror("bind");
      return(-1);
   }

   return result;
}

int Listen(int sockfd, int backlog) {
   int result;
   
   if ((result = listen(sockfd, backlog)) == -1) {
      perror("listen");
      exit(1);
   }

   return result;
}

int Accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen) {
   int connfd;

   if ((connfd = accept(sockfd, addr, addrlen)) == -1) {
      perror("accept");
      exit(1);
   }

   return connfd;
}

int Connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen) {
   int result;

   if ((result = connect(sockfd, addr, addrlen)) < 0) {
      perror("connect error");
      exit(1);
   }

   return result;
}

int Close(int fd) {
   int result;
   
   if ((result = close(fd)) < 0) {
      perror("connect error");
   }

   return result;
}

int Getsockname(int sockfd, struct sockaddr *addr, socklen_t *addrlen) {
   int result = getsockname(sockfd, addr, addrlen);
   
   if (result < 0) {
      perror("getsockname() failed");
   }

   return result;
}

struct sockaddr_in ServerSockaddrIn(int family, unsigned long ip, unsigned short port) {
   struct sockaddr_in addr;

   bzero(&addr, sizeof(addr));         
   addr.sin_family      = family;      
   addr.sin_addr.s_addr = htonl(ip);   
   addr.sin_port        = htons(port); 

   return addr;
}

struct sockaddr_in ClientSockaddrIn(int family, const char *ip, unsigned short port) {
   struct sockaddr_in addr;

   bzero(&addr, sizeof(addr));         
   addr.sin_family      = family; 
   addr.sin_port        = htons(port); 

   if (inet_pton(AF_INET, ip, &addr.sin_addr) <= 0) {
      perror("inet_pton error");
      exit(1);
   }

   return addr;
}





unsigned parseRede(char* txt, char *res, char* val){
	char *p1,*p2;
	unsigned int a,b;
	p1 = strchr(txt,':');
	p2 = strchr(txt,'#');
	if(p1==NULL || p2==NULL){printf("Erro parse %s\n",txt); return 0;}
	a = p1-txt;
	b = p2-txt-a;
	printf("parseRede:%s\n%d %d\n",txt,a,b);
	strncpy(res,txt,a);
	res[a]=0;
	strncpy(val,txt+a+1,b-1);
	val[b-1]=0;
	return p2-txt+1;
}


int criarServidorUnix(char const *socket_path){

    //Cria um socket servidor da familia UNIX.

    //char const *socket_path = "./Controlador";

    struct sockaddr_un addr;
    int fd;

    if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    	perror("Unix server socket error\n");
    	exit(-1);
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    if (*socket_path == '\0') {
      *addr.sun_path = '\0';
      strncpy(addr.sun_path+1, socket_path+1, sizeof(addr.sun_path)-2);
    } else {//todo remover
      strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);
      unlink(socket_path);
    }

    if (bind(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
    	perror("Unix server bind error");
    	exit(-1);
    }

    if (listen(fd, 20) == -1) {
    	perror("Unix server listen error");
    	exit(-1);
    }

    return fd;
}

int criarClienteUnix(char const *socket_path){
    //Cria um socket cliente da familia UNIX.

    //char const *socket_path = "./UnixSocket";

    struct sockaddr_un addr;
    int fd;

    if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
      printf("Unix client socket error");
      return -1;
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    if (*socket_path == '\0') {
      *addr.sun_path = '\0';
      strncpy(addr.sun_path+1, socket_path+1, sizeof(addr.sun_path)-2);
    } else {
      strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);
    }

    if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
    	printf("Unix client connect error");
      return -1;
    }

    return fd;
}
