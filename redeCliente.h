#ifndef mc833_redeCliente
#define mc833_redeCliente

class RedeCliente {
	
	public:
	
	int connfd; //Socket descriptor
	struct sockaddr_in connAddr; //dados do ser
		
	/*
	int carrascoConn;
	std::vector<int> listaConnUDP;
	struct sockaddr_in carrascoAddr;
	std::vector<sockaddr_in> listaAddrUDP;

*/
	RedeCliente();
	bool conectarServidor(char*, int);	
	bool enviarMsg(const char*, char*);
	bool responder(char*);
	bool enviarEscolhaJogo(ModalidadeJogo escolha);
	bool requerirAtualizacao(DadosJogo &atualiza);
	bool enviarLetra(DadosJogo &atualiza);
	bool parseAtualizaJogo(DadosJogo &atualiza, char* resposta);
	bool EstadoPreJogo();
	bool terminal(DadosJogo &atualiza);
	
	


	

};

#endif
