CFLAGS = -Wall -g -std=c++11

CODE_BOTH = interface.cpp redeCliente.cpp redeServidor.cpp socket_helper.c

all: cliente servidor

cliente: $(CODE_BOTH) cliente.cpp
	g++  $(CFLAGS) $(CODE_BOTH) cliente.cpp -o cliente


servidor: $(CODE_BOTH) servidor.cpp
	g++  $(CFLAGS) $(CODE_BOTH) servidor.cpp -o servidor



.PHONY: clean
clean:
	rm -f cliente servidor
