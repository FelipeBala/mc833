#include "headers.h"

RedeCliente::RedeCliente(){}

bool RedeCliente::conectarServidor(char* ip, int porta){
   //*port = atoi(porta);
   int result;
   this->connfd = Socket(AF_INET, SOCK_STREAM, 0);
   this->connAddr = ClientSockaddrIn(AF_INET, ip, porta);
   
   bool flag=false;
   for(int i=0;i<3;i++){
	   if ((   result =  connect(this->connfd,(struct sockaddr *) &(this->connAddr),sizeof(this->connAddr))) >= 0){
			flag=true; 
			break;
		}
		printf("Tentando conectar no servidor...\n");
		usleep(1000*1000);
   }
   return flag;
   
}


bool RedeCliente::enviarMsg(const char* msg, char *resposta){
	int n;
	if ( (n = write(this->connfd, msg, strlen(msg))) > 0 ){
		//printf("Msg Enviada:%s\n",msg);
	}else{
		//printf("Erro de envio em enviarMsg\n");
	}

	if((n = read(this->connfd, resposta, maxPacketLen-1)) >= 0) {
	  resposta[n] = 0;
	  //printf("Resposta do servidor: %s\n", resposta);
	}else{
	  //printf("Erro de recebimento em enviarMsg\n");
	}
return true;
}

bool RedeCliente::responder(char* msg){
	int n;
	if ( (n = write(this->connfd, msg, strlen(msg))) > 0 ){
		//printf("Mgs Enviada:%s\n",msg);
	}else{
		//printf("Erro de envio em responder\n");
	}
return true;
}



bool RedeCliente::enviarEscolhaJogo(ModalidadeJogo escolha){
	char mensagem[maxPacketLen], resposta[maxPacketLen];
	strcpy(mensagem, "EscolhaDoJogo:");

	switch(escolha){
		case Simples: strcat(mensagem, "Simples#");break;
		case Carrasco: strcat(mensagem, "Carrasco#");break;
		case Multiplayer: strcat(mensagem, "Multiplayer#");break;
	}

	enviarMsg(mensagem, resposta);


	switch(escolha){
		case Simples:
							if(strcmp(resposta, "OK")){
								printf("Falha ao iniciar o jogo simples\n");
								return false;
							}else{
								return true;
							}
							break;


		case Carrasco:
							if(!strcmp(resposta, "CarrascoJaEscolido")){
								printf("Ja existe um carrosco na partida./nEntre como enforcado ou espere outra sessao\n");
								return false;
							}else if(!strcmp(resposta, "VoceCarrasco")){
								printf("Voce sera o carrasco da partida\n");
								return true;
							}else{
								printf("Falha ao iniciar o jogo como carrasco\n");
								return false;
							}
							break;


		case Multiplayer:
							if(strcmp(resposta, "OK")){
								printf("Falha ao iniciar o jogo simples\n");
								return false;
							}else{
								return true;
							}
							break;
	}

return false;
}


bool RedeCliente::requerirAtualizacao(DadosJogo &atualiza){
	char resposta[maxPacketLen] = {0,0,0,0,0,0};
	enviarMsg("Letra:$#", resposta);
	bool t = parseAtualizaJogo(atualiza, resposta);
	//printf("RequerirAtualizacao - %s\n",resposta);
	return t;
}


bool RedeCliente::enviarLetra(DadosJogo &atualiza){
	char resposta[maxPacketLen];
	//////////////////////////////REVER
	enviarMsg("Letra:$#", resposta);
	return parseAtualizaJogo(atualiza, resposta);
}


bool RedeCliente::parseAtualizaJogo(DadosJogo &atualiza, char* resposta){
	char res[maxPacketLen],val[maxPacketLen];
	unsigned int lentxt = strlen(resposta);
	unsigned int i;
	while(lentxt > 0){
		if( (i = parseRede(resposta,res,val)) == 0){return false;}
		//printf("parseMsgDoServidor %d %s %s\n", i,res,val);
		lentxt -= i;
		resposta = resposta+i;
		

		if(!strcmp(res,"correta")){
			if(val[0]=='Y'){
				atualiza.correta=true;}
			else{
				atualiza.correta=false;
			}
			continue;
		}

		if(!strcmp(res,"palavra")){
			strcpy(atualiza.palavra,val);
			continue;
		}

		if(!strcmp(res,"tamanhoPalavra")){
			atualiza.tamanhoPalavra = atoi(val);
			continue;
		}

		if(!strcmp(res,"vidas")){
			atualiza.vidas = atoi(val);
			continue;
		}

		if(!strcmp(res,"pessoaQueEscolheu")){
			strcpy(atualiza.pessoaQueEscolheu,val);
			continue;
		}

		if(!strcmp(res,"proximaPessoa")){
			strcpy(atualiza.proximaPessoa,val);
			continue;
		}

		if(!strcmp(res,"msgServidor")){
			strcpy(atualiza.msgServidor,val);
			continue;
		}

		if(!strcmp(res,"letra")){
			atualiza.letra = val[0];
			continue;
		}
		
		
		printf("ERRRO - parseMsgDoServidor %s %s\n",res,val);
		return false;
	}

return true;

}



bool RedeCliente::EstadoPreJogo(){
	while(true){
		printf("EstadoPreJogo");
		char resposta[maxPacketLen];
		enviarMsg("Tempo", resposta);
		if(!strcmp(resposta,"SemCarrasco")){
			return false;
		}
		if(!strcmp(resposta,"IniciarPartidaMultiplayer")){
			return true;
		}
		printf("%s\n", resposta);
		usleep(1000*1000);
	}

return false;
}



bool RedeCliente::terminal(DadosJogo &atualiza){


	char   message[maxPacketLen];
	char   response[maxPacketLen];
	int    n, rv;



	// Estrutura de dados para definir quais decritores o poll ira tratar e para quais
	// Eventos ele ficara atento.
	struct pollfd ufds[2];
	ufds[0].fd = this->connfd;
	ufds[0].events = POLLIN ;
	ufds[1].fd = (long) fopen ("stdin","rw");
	ufds[1].events = POLLIN;


	while(true){
	// Espera por um ou mais evento dos descritores selecionados. Se a espera alcancar 1 segundo
	// indica um timeout retornando 0 para a variavel rv. Em caso de erro retorna 0.
		rv = poll(ufds, 2, 500);
		if (rv == -1) {
			// Ocorreu um erro no poll
			printf("Erro no poll\n");}
		else if (rv == 0) {
			//Time-Out
			//printf("Timeout !  Sem dados apos espera de %d segundos.\n",TempoEspera);'
		  }
		else {
			// Verifica os eventos
			// Verifica se ha pacotes para ler no socket.
			if (ufds[0].revents & POLLIN || ufds[0].revents) {
				if( (n = read(this->connfd, response, maxPacketLen-1)) > 0){
					response[n]=0;
					printf("%s", response);
					parseAtualizaJogo(atualiza, response);
					return true;
				}
			 }

			// Verifica se ha dados de entrada na entrada padrao (stdin).
			if (ufds[1].revents & POLLIN) {
			// Se receber um EOF do stdin, fgets retorna zero.
				if(fgets (response, maxPacketLen-1, stdin)) {
					if(response[0]=='@'){
						return true;
					}

					if(response[0]=='\n'){
						return true;
					}

					if(!strcmp(response,"sair")){
						return false;
					}

					if(strlen(response) < 3){
						strcpy(message,"Letra:");
						response[0] = 	toupper(response[0]);
						response[1]=0;
						strcat(message,response);
						strcat(message,"#");
					}else{
						strcpy(message,"Palavra:");
						strcat(message,response);
						strcat(message,"#");
					}

					printf("Terminal - msg enviada: %s\n", message);
					responder(message);
				}

			}

		}

	}




}








